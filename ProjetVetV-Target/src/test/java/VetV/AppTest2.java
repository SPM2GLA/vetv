package VetV;


import VetV.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Unit test for simple App.
 */
public class AppTest2 
extends TestCase
{
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest2( String testName )
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( AppTest2.class );
	}

	/**
	 * test2 de visualisation de graph
	 * @throws Exception 
	 */
	public void test2() throws Exception
	{
		SampleDataJpaApplication s = new SampleDataJpaApplication();
		s.start();


		Runtime r = Runtime.getRuntime();
		for (int i = 0; i < 1; i++) {

			
			Process p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/create/email@Test1/Prof1");p.waitFor();
			p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/create/email@Test2/Prof2");p.waitFor();
			p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/createAccount/Yousra/email@Test2");p.waitFor();
			p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/get-by-email/email@Test1");p.waitFor();			
			p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/update/6/NewEmail@Test1/Prof40");p.waitFor();
			p = r.exec("curl -H 'Content-Type: application/json' -X GET http://localhost:8080/delete/2");p.waitFor();
			
			BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";

			while ((line = b.readLine()) != null) {
				System.out.println(line);
			}

			b.close();
		}
		Thread.sleep(15000);
		assert(true);
	}
}
