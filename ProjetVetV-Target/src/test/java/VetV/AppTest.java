package VetV;


import VetV.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Unit test for simple App.
 */
public class AppTest 
extends TestCase
{
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest( String testName )
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( AppTest.class );
	}

	/**
	 * test1 de visualisation de graph
	 * @throws Exception 
	 */
	public void test1() throws Exception
	{
		SampleDataJpaApplication s = new SampleDataJpaApplication();
		s.start();


		Runtime r = Runtime.getRuntime();
		for (int i = 0; i <10 ; i++) {

			Process p = r.exec("../curl.sh");
			p.waitFor();
			BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";

			while ((line = b.readLine()) != null) {
				System.out.println(line);
			}

			b.close();
		}
		Thread.sleep(15000);
		assert(true);
	}
	
}
