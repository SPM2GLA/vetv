package VetV.service;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import VetV.domain.User;


// Imports ...

@Transactional
public interface UserDao extends JpaRepository<User, Long> {
	/**
	   
	   * Note that those methods are not implemented and their working code will be
	   * automagically generated from their signature by Spring Data JPA.
	   */
  /**
   * This method will find an User instance in the database by its email.
   
   */
  public User findByEmail(String email);
  /**
   * This method will find an User instance in the database by its id.
   
   */
  public User findById(long id);
  /**
   * This method will find an User instance in the database by its name.
   
   */
  public User findByName(String name);
  


}