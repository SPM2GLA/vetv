package VetV.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import VetV.domain.Account;
import VetV.domain.User;


// Imports ...


@Transactional
public interface AccountDao extends JpaRepository<Account, Long> {
	



	
		/**
		   
		   * Note that those methods are not implemented and their working code will be
		   * automagically generated from their signature by Spring Data JPA.
	
	  /**
	   * This method will find an User instance in the database by its id.
	   
	   */
	  public Account findById(long id);
	  /**
	   * This method will find an User instance in the database by its name.
	   
	   */
	  public Account findByUser(User user);
	  


	}


