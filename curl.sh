#!/bin/bash
cd hsdbserver
export PATH=$PATH:`pwd`

if pgrep -f "hsqldb" > /dev/null 
then
	echo "Running"
	echo 
	echo 
	echo curl -H Content-Type: application/json -X GET http://localhost:8080/test
	sleep 1
	echo 
	curl -H 'Content-Type: application/json' -X GET http://localhost:8080/test
	echo 
	curl -H 'Content-Type: application/json' -X GET http://localhost:8080/delete/60
	echo 
else
	echo "Starting"
	rm -rf ./data/*
	if [ ! -d "data" ];
	then
	#if directory doesn't exist
		mkdir data
	fi
	xterm -e ./run-hsqldb-server.sh&
	sleep 6
	echo "Now restart the java project and do it again"
fi
