package VetV.monitor;

import javax.annotation.PostConstruct;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import VetV.visualization.Visualization;
import VetV.visualization.VisualizationMatrix;

@Aspect
@Component
@Configuration
@EnableAspectJAutoProxy
public class ServiceMonitor {
	private Visualization visualization;
	private VisualizationMatrix visualisationMatrix;
	
	
	
	@Before("execution(* *.*.*.*(..))")
	public void BeforeMethod(JoinPoint pjp) {
		Signature sign = pjp.getSignature();
		String concreteClass = sign.getDeclaringTypeName();//getTarget().getClass() 
		System.err.println(" caller "+concreteClass +"  calling  "+ pjp.toString());
		
		visualisationMatrix.parserPackage(pjp.toString().split(" ", 2)[1]);
		visualisationMatrix.enterCall(pjp.toString().split(" ", 2)[1]);
	}
	@After("execution(* *.*.*.*(..))")
	public void afterMethod(JoinPoint pjp) {
		Signature sign = pjp.getSignature();
		String concreteClass = sign.getDeclaringTypeName();//getTarget().getClass() 
		System.err.println("stop caller "+concreteClass +"  calling  "+ pjp.toString());
		visualisationMatrix.leaveCall();
	}
	
	
	
	
	
	/** COMMENTER TOUT DECOMMENTER UNE SEUL DES DEUX FONCTION **/
	
	/** fonction d'affichage du graph **/
//	int nbToPG = 0;
//	@Before("execution(* VetV.*.*.*(..))")
//	public void logServiceAccess4(JoinPoint joinPoint) {
//	
//		
//		System.err.println(" " + joinPoint.toString().split(" ", 2)[1]);
//		
//		visualization.parserPackage(" " + joinPoint.toString().split(" ", 2)[1]);
//	}

	/** FONCTION AFFICHAGE INIT SPRING CRASH APRES LANCEMENT MAIS JOLIE POUR LA DEMO **/
	@Before("execution(* *.*(..))")
	public void logServiceAccess5(JoinPoint joinPoint) {
		visualization.parserPackage(" " + joinPoint.toString().split(" ", 2)[1]);
		/** visualization.exportImage(); **/ //ne pas exporter l'image car trop long a faire
	}
	
	
	@After("execution(* VetV.web.UserController.*(..))")
	public void logServiceAccess(JoinPoint joinPoint) {
		System.err.println("Completed: " + joinPoint);
	}
	
	public void setVisualization(Visualization v){
		this . visualization = v;
	}
	
	@PostConstruct
	private void init(){System.err.println("init");
		if(visualization == null) visualization = new Visualization();
		if(visualisationMatrix == null) visualisationMatrix = new VisualizationMatrix();
	}
	public void exportImage(){
		System.out.println("serviceMonitor: export IMG");
		visualization.exportImage();
	}
}