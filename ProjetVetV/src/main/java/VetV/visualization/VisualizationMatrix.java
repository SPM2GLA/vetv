package VetV.visualization;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.graphstream.graph.Node;
import org.graphstream.stream.file.FileSinkImages;

import javafx.scene.paint.Color;

//affiche une matrice methode*methode avec comme valeur le nombre d'appel a cette methode depuis la methode précedente
public class VisualizationMatrix {
	private List<List<Integer>> dynamicMatrix 	= new ArrayList<List<Integer>>();	//ligne colonne
	private List<String> 		names 			= new ArrayList<String>();
	private LinkedList<Integer> callsIndex = new LinkedList<Integer>();
	public static void main(String args[]) {
		
		VisualizationMatrix nx = new VisualizationMatrix();
		nx.showMatrix();
		nx.addMethodToMatrix("Pate à crepe");
		nx.showMatrix();
		nx.addMethodToMatrix("Nutella");
		nx.showMatrix();
//		nx.addMethodToMatrix("A");
//		nx.addMethodToMatrix("B");
//		nx.addMethodToMatrix("Get");
//		nx.addMethodToMatrix("Socket");
		nx.addMethodToMatrix("Pomme");
		nx.showMatrix();
		nx.showMatrixSWING();
		nx.parserPackage("Nutella");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.enterCall("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.leaveCall();
		nx.enterCall("Pomme");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Nutella");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pate à crepe");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.parserPackage("Pomme");
		nx.exportImage();
	}
	
	//retrourne l'index de la methode passer en parametre dans la liste de nom
	//si n'existe pas ajoutet la methode et retourne le nouvelle index
	private int getMatriceIndex(String methode){
		System.out.println(">>>getMatriceIndex");
		for (int i = 0; i < names.size(); i++) {
			if(methode.equals(names.get(i))){
				System.out.println("find index : "+ i);
				return i;
			}
		}
		System.out.println("pas trouvé ajout dans la liste de : "+methode);
		addMethodToMatrix(methode); // si non present dans la lsit le rajouter
		return names.size()-1;
	}
	
	//methode qui ajoute a un filo l'index de la metode courrante, pour faire le lien vers la methode suivante (de ou vers ou)
	public void enterCall(String methode){
		System.out.println(">>>enterCall");
		System.err.println("*");for (Iterator iterator = callsIndex.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.print(" "+names.get(integer)+"->");
		}System.out.print(" \n");
		callsIndex.add(getMatriceIndex(parserMethodename(methode)));
		System.err.println("*");for (Iterator iterator = callsIndex.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.print(" "+names.get(integer)+"->");
		}System.out.print(" \n");
	}
	
	//methode qui dépille l'index de la dernière methode que l'on viens de quitter
	//l'index est donc sur la précedente méthode qui appel
	//retourne false si pas d'éléments de niveau supérieur
	public boolean leaveCall(){
		System.out.println(">>>leaveCall");
		System.out.print("\n");
		if(callsIndex.size()>0){
			callsIndex.removeLast();
			System.err.println("*");for (Iterator iterator = callsIndex.iterator(); iterator.hasNext();) {
				Integer integer = (Integer) iterator.next();
				System.out.print(" "+names.get(integer)+"->");
			}System.out.print(" \n");
			return true;
		}
		else{
			System.err.println("*");for (Iterator iterator = callsIndex.iterator(); iterator.hasNext();) {
				Integer integer = (Integer) iterator.next();
				System.out.print(" "+names.get(integer)+"->");
			}System.out.print(" \n");
			return false;
		}
	}
	
	//methode pour ajouter un appel entre 2 methodes
	//a la fin ajoute la methode dans la pilel d'appel
	public void parserPackage(String inputString){
		System.out.println(">>>parserPackage");
		//recup ou ajout de la methode dans la matrice
		int indexMethode = getMatriceIndex(parserMethodename(inputString));
		increaseMethode(indexMethode);
		showMatrix();
		showMatrixSWING();
		timerExportImage();
	}
	private String parserMethodename(String inputString){
		System.out.println(">>>parserMethodename");
		while(inputString.subSequence(0, 1).equals(" "))
			inputString = inputString.substring(1, inputString.length());
		String[] tabInputString = inputString.split("\\.", 200);
		System.out.println("tab size :"+tabInputString.length);
		System.out.println("parsing : "+inputString);
		System.out.println("returning : "+tabInputString[tabInputString.length-1]);
		return tabInputString[tabInputString.length-1];
	}
	//augmente le nombre d'appel de 1 pour la methode matrice[indexe][CallsIndex]
	private void increaseMethode(int index){
		System.out.println(">>increaseMethode");
		showMatrix();
		System.out.println("index: " +index);

		if(!callsIndex.isEmpty()){
			System.out.println("call index plein "+callsIndex.size());
			ListIterator<Integer> it = dynamicMatrix.get(callsIndex.peek()).listIterator(index);it.next();
			it.set(dynamicMatrix.get(callsIndex.peek()).get(index) +1);
		}else{
			System.out.println("call index vide, ajout de  [0]["+index+"]+1");
			ListIterator<Integer> it = dynamicMatrix.get(0).listIterator(index);it.next();
			it.set(dynamicMatrix.get(0).get(index) +1);
		}
	}


	public VisualizationMatrix() {
		super();
		System.out.println(">>VisualizationMatrix");
		addMethodToMatrix("main");
		showMatrixSWING();
		// TODO Auto-generated constructor stub
	}

	// ajoute la methode dans la matrice uniquement
	public void addMethodToMatrix(String methode){
		System.out.println(">>addMethodToMatrix");
		System.out.println("   add "+methode);
		names.add(methode);
		
		for (Iterator iterator = dynamicMatrix.iterator(); iterator.hasNext();) {//pour toutes les ligne, ajouter une colonne 0
			List<Integer> list = (List<Integer>) iterator.next();
			list.add(0);
		}
		
		List newList = new ArrayList<Integer>(); // ajouter une ligne
		for(int i = 0; i < names.size(); i++){//ajout de  size colonne dans la ligne
			newList.add(0);
			
		}
		
		dynamicMatrix.add(newList);
		
	}
	//affiche txt de la matrice
	private void showMatrix(){
		System.out.println(">>showMatrix");

		System.out.println("names.size "+names.size());
//		System.out.println("dynamicMatrix.size "+dynamicMatrix.size());
//		if(dynamicMatrix.size() > 0 && dynamicMatrix.get(0) != null)System.out.println("dynamicMatrix.get(0).size "+dynamicMatrix.get(0).size());
		
		for(int i = -1; i < names.size(); i++){
			if(i == -1){
				System.out.print("         ");
				for(int j = 0; j < names.size(); j++){
					System.out.print(" "+names.get(j)+" |");
				}
				System.out.println("");
			}
			else{
				for(int j = -1; j < names.size(); j++){
					if(j == -1)
						System.out.print(names.get(i)+" | ");
					else
						System.out.print(" "+dynamicMatrix.get(i).get(j)+" |");
				}
				System.out.println("");
			}
		}
		
	}
	//nope
	private void addMethod(){
		System.out.println(">>addMethod");
	
	dynamicMatrix.add(new ArrayList<Integer>());
	dynamicMatrix.add(new ArrayList<Integer>());
	dynamicMatrix.add(new ArrayList<Integer>());

	dynamicMatrix.get(0).add(6);
	dynamicMatrix.get(0).add(7);
	dynamicMatrix.get(0).add(8);

	System.out.println(dynamicMatrix.get(0).get(0)); // 6
	System.out.println(dynamicMatrix.get(0).get(1)); // 7
	System.out.println(dynamicMatrix.get(0).get(2)); // 8

	}
	private boolean init = false;
	JFrame frame;
	private JPanel contentPane;
	GridLayout experimentLayout;
	private void showMatrixSWING(){
		if(!init){
			init=true;
			frame = new JFrame("HelloWorldSwing");
			frame.setSize(300, 300);
			final JLabel label = new JLabel("Hello World");
			
			experimentLayout = new GridLayout(1,0);
			contentPane = new JPanel(experimentLayout);
			frame.add(contentPane,BorderLayout.CENTER);
		}
		contentPane.removeAll();
		experimentLayout.setColumns(names.size()+1);
		experimentLayout.setRows(names.size()+1);
	    contentPane.add(new JLabel(""));
	    //recherche du total et du max pour faire la moyenne
	    int max =0;
	    int nb = 0;//nb de cellule pas à 0
		for (int i = 0; i < names.size(); i++) {
			for (int j = 0; j < names.size(); j++) {
				if(dynamicMatrix.get(i).get(j) != 0)
					nb++;
					if (dynamicMatrix.get(i).get(j) > max )
						max = dynamicMatrix.get(i).get(j);
					
				
			}
		}
	    
	    //crée le tableau
		for (int i = 0; i < names.size(); i++) {
			contentPane.add(new Button(names.get(i)));
		}
		for (int i = 0; i < names.size(); i++) {
			for (int j = -1; j < names.size(); j++) {
				if(j<0)
					  contentPane.add(new Button(names.get(i)));
				else{
					int value = dynamicMatrix.get(i).get(j);
					if(dynamicMatrix.get(i).get(j) == 0)
						contentPane.add(new JLabel(""));//ajout vide
					else{
						Button b = new Button(""+dynamicMatrix.get(i).get(j));
						int hex = (dynamicMatrix.get(i).get(j)*255)/max;
						String hexx = Integer.toHexString(i);
						System.out.println("Hexx : max :"+max+" cell:"+dynamicMatrix.get(i).get(j)+" hex:"+hex+" hexx:" + hexx);
						b.setBackground( java.awt.Color.decode("0x00"+Integer.toHexString(hex)+"00"));
						contentPane.add(b);
					}
				}

			}
			
		}
		    
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.pack();
		    frame.setVisible(true);
		    frame.repaint();
	}
	
	private Clock clock = new Clock(this::exportImage);
	public void timerExportImage(){
		clock.start(5000);//demande l'exportation après 5s puis toutes les 50s
		//resset a chaque appel de cette méthode
	}
	
	public void exportImage(){
		System.out.println("visualizationMatrix: export IMG"+"Visualization-Matrix-"+System.currentTimeMillis()+".png");
		frame.repaint();
		contentPane.repaint();
		BufferedImage img = new BufferedImage(contentPane.getWidth(), contentPane.getHeight(), BufferedImage.TYPE_INT_RGB);
		contentPane.setSize(100, 100);
		frame.getContentPane().paintAll(img.createGraphics());
	    try {
	        ImageIO.write(img, "png", new File(System.currentTimeMillis()+"Visualization-Matrix-"+".png"));
	        System.out.println("panel saved as image");

	    } catch (Exception e) {
	        System.out.println("panel not saved" + e.getMessage());
	    }
	}
}