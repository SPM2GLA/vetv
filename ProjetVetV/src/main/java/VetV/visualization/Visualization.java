package VetV.visualization;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.ProxyPipe;
import org.graphstream.stream.SinkAdapter;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

//affiche un graph qui décrit la structure de l'appication et permet d'afficher en temps réel les appels des méthodes
public class Visualization implements ViewerListener, MouseMotionListener {
	//		public static void main(String args[]) {
	//			Graph graph = new SingleGraph("Tutorial 1");
	//			
	//			graph.setStrict(false);
	//			graph.setAutoCreate( true );
	//			graph.addNode("A" );
	//			graph.addNode("B" );
	//			graph.addNode("C" );
	//			graph.addEdge("AB", "A", "B");
	//			graph.addEdge("BC", "B", "C");
	//			graph.addEdge("CA", "C", "A");
	//
	//			graph.display();
	//
	//
	//		}

	private boolean init = false;
	private Graph graph = null;
	private Viewer viewer = null; 
	private ViewerPipe fromViewer = null;
	private Queue<Node> ColloredNodes = new LinkedList<Node>();
	protected boolean loop = true;
	private boolean cleanningColor = false;
	public void parserPackage(String inputString){
		// enlever la couleur des node après un temps
		if(!cleanningColor){
			cleanningColor = true;
		new java.util.Timer().schedule( 
				new java.util.TimerTask() {

					public void run() {
						while (! ColloredNodes.isEmpty()){

							Node rc = ColloredNodes.remove();
							rc.setAttribute("ui.style", "fill-color: rgb(0,255,100);");
							if(rc.hasAttribute("collapsed"))
								rc.setAttribute("ui.style", "fill-color: rgb(255,0,0);");
							for (Iterator iterator = ColloredNodes.iterator(); iterator.hasNext();) {
								Node node = (Node) iterator.next();
								node.setAttribute("ui.style", "fill-color: rgb(0,100,255);");
							}
							try {
								Thread.sleep(60);
							} catch (InterruptedException e) {
							}							
						}
						cleanningColor = false;
					}
				}, 
				200
				);
		}
		//**** pour chaque chemin : VetV.web.UserController.getAll()) (par exemple)
		//*** extraire juste la chaine de caractères sans l'espace 
		while(inputString.subSequence(0, 1).equals(" "))
			inputString = inputString.substring(1, inputString.length());
		if(!init) init();
		//***** divier le chemin à un tableau de mots
		String[] tabInputString = inputString.split("\\.", 200);
//		System.out.println("tab size :"+tabInputString.length);
//		System.out.println("parsing : "+inputString);
		//****ici pour chaque mot du tableau dans notre cas Vet par exemple
		for (int i = 0; i < tabInputString.length; i++) {
			
			/** THEAR.SLEEP SI CAUSE PROBLEMES OU DEMO COMMENTER **/
//			try {
//				Thread.sleep(50);
//			} catch (InterruptedException e) {
//				
//				e.printStackTrace();
//			}

//			System.out.println("processing : "+tabInputString[i]);
			Node n = graph.getNode(tabInputString[i]);
			//si le noeud n'existe pas avant
			if(n == null){
				//***on l'ajoute dans le graphe et on lui ajoute un attribut
				n = graph.addNode(tabInputString[i]);
				n.addAttribute("place", i+1);
			}
			//****si le noeud existe déjà et a un label on incrémente son label
			if(n.hasAttribute("ui.label"))
				increaseNode(n);//n.setAttribute("ui.label",n.getAttribute("ui.label")+"|");
			else
				//**** si le noeud n'a jamais reçu d label on lui en crée un 
				n.addAttribute("ui.label",tabInputString[i]+" 1");
			n.addAttribute("ui.style", "fill-color: rgb(0,100,255);");
			ColloredNodes.add (n);

			if(i==0) // si c'est le premier noeud, on le lie a START
				graph.addEdge("START_"+tabInputString[i], "START", tabInputString[i],true);
			else// sinon on le lie au noeud du dessus.
				graph.addEdge(tabInputString[i-1]+"_"+tabInputString[i], tabInputString[i-1], tabInputString[i],true);

			n.addAttribute("ui.color", Color.RED);
			graph.getNode(tabInputString[i]).addAttribute("fill-mode", "dyn-plain");
			graph.getNode(tabInputString[i]).addAttribute("ui.color", Color.RED);
		}


		timerExportImage();
	}

	public Visualization() {
		super();
		init();
		// TODO Auto-generated constructor stub
	}

	private void increaseNode(Node node){
		try {
			int i = Integer.valueOf(node.getAttribute("ui.label").toString().split(" ")[1]);
			String s =node.getAttribute("ui.label").toString().split(" ")[0];
			node.setAttribute("ui.label",s+" "+ ++i);
		} catch (Exception e) {
		}
	}

	public void init(){
		if(!init){
			init = true;
			graph = new SingleGraph("Visualization");
			//graph.display();
			graph.setStrict(false);
			graph.setAutoCreate( true );

			viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
			View view = viewer.addDefaultView(true);   // false indicates "no JFrame".


			fromViewer = viewer.newViewerPipe();
			fromViewer.addViewerListener(this);
			fromViewer.addSink(graph);
			viewer.enableAutoLayout();
			view.addMouseMotionListener(this); 
			graph.addNode("START");
			graph.getNode("START").addAttribute("place", 0);
			loopInANewThread();
		}
	}

	private void loopInANewThread(){
		Thread t1 = new Thread(new Runnable() {
		     public void run() {
		          // code goes here.
		    	 while(loop) {
//		    		 System.out.println("loop!");
		    		 try {
		    			 fromViewer.blockingPump();
		    		 } catch (InterruptedException e) {
		    			 // TODO Auto-generated catch block
		    			 e.printStackTrace();
		    		 }
		    	 }
		     }
		});  
		t1.start();
		
	}
	private void foldUnfold(Node node){
		// let node be the clicked node...
		Iterator<Node> it = node.getBreadthFirstIterator(true);
		//********J'ai pas compris cet attribut collapsed : il est defini ou 
		//****Si le noeud mjmo3  
		if(node.hasAttribute("collapsed")){
			node.removeAttribute("collapsed");
			//***une fois on le clique il devient vert
			node.setAttribute("ui.style", "fill-color: rgb(0,255,100);");
			//***mais s'il est un noeud initial il devient noir
			if(node.equals(graph.getNode("START")))
				node.setAttribute("ui.style", "fill-color: rgb(0,0,0);");
			//**** si ce noeud a un noeud suivant 
			while(it.hasNext()){
				Node m  =  it.next();
		//*****on fait apparaite le edge et on continue la boucle pour faire apparaitre	
		//tous les noeuds et edges cachés
				for(Edge e : m.getLeavingEdgeSet()) {
					e.removeAttribute("ui.hide");
				}
				if(node != m) {
					m.removeAttribute("ui.hide");
					m.removeAttribute("collapsed");
					m.setAttribute("ui.style", "fill-color: rgb(0,255,100);");
				}
			}
		}
		//*****Sinon si le noeud n'est pas mjmo3 donc haykon f lvert hanrdoh f lrouge
		else{
			node.setAttribute("collapsed");
			//*****on le rend rouge
			node.setAttribute("ui.style", "fill-color: rgb(255,0,0);");
			//***si ce noeud a des noeud suivants
			while(it.hasNext()){
				Node m  =  it.next();
					for(Edge e : m.getLeavingEdgeSet()) {
						//**** on cache tous les edges pour obtenir à la fin un noeud rouge mjmo3
						e.setAttribute("ui.hide");
					}
					if(node != m) {
						m.setAttribute("ui.hide");
					}
					
			}
		}
	}

	public void buttonPushed(String id) {
		System.out.println("Button pushed on node "+id);
//		graph.getNode("START").addAttribute("ui.label","I clicked !");
//		graph.getNode(id).setAttribute("ui.label","I clicked !");
		
		movedwhileclicked = false;
	}

	public void buttonReleased(String id) {
		System.out.println("Button released on node "+id);
		if(!movedwhileclicked)
			foldUnfold(graph.getNode(id));
	}
	private boolean movedwhileclicked = false;
	public void viewClosed(String id) {
		loop = false;

	}

	public void mouseDragged(MouseEvent e) {
		System.out.print("*");
		// TODO Auto-generated method stub
		movedwhileclicked=true;
		
	}

	public void mouseMoved(MouseEvent e) {
		System.out.print(".");
		// TODO Auto-generated method stub
		
	}
	private Clock clock = new Clock(this::exportImage);
	public void timerExportImage(){
		clock.start(5000);//demande l'exportation après 5s puis toutes les 50s
		//resset a chaque appel de cette méthode
	}
	public void exportImage(){
		System.out.println("visualization: export IMG"+"Visualization-Graph-"+System.currentTimeMillis()+".png");
		 FileSinkImages pic = new FileSinkImages(FileSinkImages.OutputType.PNG, FileSinkImages.Resolutions.VGA);
		 
		 pic.setLayoutPolicy(FileSinkImages.LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);
		 try {
			pic.writeAll(graph, System.currentTimeMillis()+"Visualization-Graph-"+".png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


