package VetV.visualization;

import static java.util.concurrent.TimeUnit.*;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class Clock {
	private ScheduledExecutorService scheduler;
	private Commande cmd;

	public Clock(Commande cmd) {
		System.out.println("clock Clock(cmd): "+cmd);
		this.cmd = cmd;
	}
	//perio, temps en millisecondes entre 2 appel de la commande
	public void start(long period) { 
		System.out.println("clock start(): "+period);
		if (scheduler != null) { // si il y a deja un thread running on le stop
			this.stop();
			this.scheduler = Executors.newScheduledThreadPool(1);
		}
		this.scheduler = Executors.newScheduledThreadPool(1);
		System.out.println("clock cmd:"+cmd);
		scheduler.scheduleAtFixedRate(cmd::execute, period, period*10, MILLISECONDS); // execute la cmd passer dans le constructeur toutes les periodes millisecondes
		//		cmd::execute, lambda expression qui dit appeler execute de cmd
	}

	public void stop(){
		if(scheduler != null)
			scheduler.shutdownNow(); // ~scheduler = nuul
	}
}