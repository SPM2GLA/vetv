package VetV.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import VetV.domain.User;
import VetV.domain.Verification;
import VetV.service.UserDao;


@Controller
public class UserController {
	@Autowired
	private UserDao userDao;
	@Autowired
	private User user;
   @Autowired
   private Verification verif;



	/**
	 * GET /create  --> Create a new user and save it in the database.
	 */
	@RequestMapping("/create/{email}/{name}")
	@ResponseBody
	public String REST_create(@PathVariable("email")String email,@PathVariable("name")String name) {



		String userId = "";
		System.err.println("adding "+email+" "+verif.upperName(name));
		try {
			//test si existe
			if(verif.verifyEmail(email)){
				User user = userDao.findByEmail(email);
				if (user != null){
					userId = String.valueOf(user.getId());
					return "User already present with id = " + userId;
				}
				else {
					User user2 = new User(email,verif.upperName(name));
					userDao.save(user2);
					userId = String.valueOf(user2.getId());
					return "User succesfully created with id = " + userId;

				}}
		}
		catch (Exception ex) {
			return "Error creating the user: " + ex.toString();
		}
		return "";
	}




	/**
	 * GET /delete  --> Delete the user having the passed id.
	 */
	@RequestMapping("/delete/{id}")
	public String REST_delete(@PathVariable("id")long id) {
		try {
			//TestAOP test=new TestAOPImpl();
			//User userr=user.SuperUseLessFunction1(id);
			User userr=userDao.findById(user.SuperUseLessFunction1(id));
			if(userr != null)
				userDao.delete(userr);
		}
		catch (Exception ex) {
			return "Error deleting the user:" + ex.toString();
		}
		return "User succesfully deleted!";
	}

	/**
	 * GET /get-by-email  --> Return the id for the user having the passed
	 * email.
	 */
	@RequestMapping("/get-by-email/{email}")
	@ResponseBody

	public String REST_getByEmail(@PathVariable("email")String email) {
		String userId = "";
		try {

			User user = userDao.findByEmail(email);

			userId = String.valueOf(user.getId());
		}
		catch (Exception ex) {
			return "User not found";
		}
		return "The user id is: " + userId;
	}

	/**
	 * GET /update  --> Update the email and the name for the user in the 
	 * database having the passed id.
	 */
	@RequestMapping("/update/{id}/{email}/{name}")
	@ResponseBody


	public String REST_updateUser(@PathVariable("id")long id,@PathVariable("email") String email,@PathVariable("name") String name) {
		try {
			User user = userDao.findById(id);
			if(user!=null && verif.verifyEmail(email))
			user.setEmail(email);
			user.setName(verif.upperName(name));
			userDao.save(user);
		}
		catch (Exception ex) {
			return "Error updating the user: " + ex.toString();
		}
		return "User succesfully updated!";
	}

	/**
	 * Function --> Create a list of users and save it in the database.
	 */
	public  void createUsers(){
		User user1=user.createNewUser("yousraelghzizal@hotmail.fr","yousra");
		User user2=user.createNewUser("StephenPiton@hotmail.fr","stephen");
		User user3=user.createNewUser("toto@hotmail.fr","toto");
		userDao.save(user1);
		userDao.save(user2);
		userDao.save(user3);
	}

	/**
	 * GET /create  --> get all the users from the database.
	 */
	@RequestMapping("/test")
	@ResponseBody
	public  List<User> REST_getAll(){
		if(userDao.findAll()==null){
		createUsers();
		return  userDao.findAll();}
		else
		return  userDao.findAll();
	}
	


}