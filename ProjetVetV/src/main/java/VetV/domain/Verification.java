package VetV.domain;

import org.springframework.stereotype.Component;

@Component
public class Verification {
	/**
	   * This boolean method will verify if the syntax of an email is correct or not.
	   
	   */
	public boolean verifyEmail(String email){

		if(email ==null || ((!email.contains("@")) || (!email.contains("."))) ){

			try {
				throw new Exception("Veuillez entrer un  email valide");
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			return false;
		}
		else
			return true;
	}
	/**
	   * This method will transform the name of a user from lower to upper case.
	   
	   */
	public String upperName(String nameUser){

		return nameUser.toUpperCase();
	}
}
